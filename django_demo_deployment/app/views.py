from django.shortcuts import render
from django.http import HttpResponse
def main(request):
        # return HttpResponse('<p>hello</p>')

        # return render(request, 'main.html')

    origin = request.GET.get('origin', None)
    destination = request.GET.get('destination', None)
    steps = ['step0', 'step1', 'step2']
    content = {
        'origin': origin,
        'destination': destination,
        'steps': steps
     }
    return render(request, 'main.html', content)
